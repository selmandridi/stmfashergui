import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from pystlink import *
import binascii



class Example(QMainWindow):
    
    def __init__(self):
        super().__init__()

        self.binaryFileText = QTextEdit(self)
        self.serialText = QTextEdit(self)
        self.outputText = QTextEdit(self)
        
        self.progressBar = QProgressBar(self)
        self.progressBar.setGeometry(QRect(100, 320, 280, 20))

        self.programLabel = QLabel("",self)
        self.programLabel.setGeometry(QRect(280, 230, 250, 20))
        self.programLabel.setStyleSheet('color: red; font: bold')     

        self.msg = QMessageBox(self)
        self.msg.setIcon(QMessageBox.Critical)
        self.msg.setText("Error message:")
        self.msg.setWindowTitle("ERROR !")
        self.msg.setStandardButtons(QMessageBox.Ok)

        self.fileName = ""
        self.myserialtext = ""
        self.allowSerilaFlash = False

        self.pystlink = PyStlink()
        self.pystlink._dbg = self
        
        self.setupUi()      
        
    def setupUi(self):

        
        self.progressLabel = QLabel("Progress: ",self)
        self.progressLabel.setGeometry(QRect(10, 320, 250, 20))
        
        labSerialNum = QLabel("Serial Number:", self)
        labSerialNum.setGeometry(QRect(10, 10, 141, 31))        
        labSerialNum.setObjectName("labSerialNum")

        logLabel = QLabel("Log Output:", self)
        logLabel.setGeometry(QRect(10, 125, 141, 31))        

        labFileBinary = QLabel("Binary File:", self)
        labFileBinary.setGeometry(QRect(10, 70, 141, 31))
        labFileBinary.setObjectName("labFileBinary")

        self.serialText.setGeometry(QRect(10, 35, 200, 31))
        self.serialText.setObjectName("serialText")

        self.outputText.setGeometry(QRect(10, 150, 250, 150))
        self.outputText.setObjectName("outputText")

        self.binaryFileText.setGeometry(QRect(10, 95, 200, 31))
        self.binaryFileText.setObjectName("binaryfileText")

        btnProgram = QPushButton("Program", self)
        btnProgram.move(280, 150)

        btnConnect = QPushButton("Connect", self)
        btnConnect.move(280, 35)

        btnBrowse = QPushButton("Browse", self)
        btnBrowse.move(280, 95)

        btnConnect.clicked.connect(self.buttonConnectClicked)
        btnProgram.clicked.connect(self.buttonProgramClicked)
        btnBrowse.clicked.connect(self.openFileNameDialog)          
        
        self.setGeometry(300, 300, 400, 350)
        self.setWindowTitle('STM Flasher')
        self.show()


    def buttonConnectClicked(self):    

        self.pystlink.detect_cpu('', False)

        self.pystlink.cmd(['dump','0x1FFF7800']) #0x1FFF7800 
        
    def buttonProgramClicked(self):    
        
        if(self.allowSerilaFlash):


            reply = QMessageBox.question(self, 'Continue?', 
            'Are you sure you want to the Flash the Serial Number ?\n\nOnce the Serial Flashed the address is locked and cannot be modified ever!', QMessageBox.Yes, QMessageBox.No)

            if reply == QMessageBox.Yes:
                
                f = open('serial.bin', 'wb')

                self.myserialtext = self.serialText.toPlainText().split('x')

                for i in range(len(self.myserialtext[1]),0,-2) :

                    #print("test :",i,self.myserialtext[1][i-2:i])
                
                    f.write(binascii.unhexlify(self.myserialtext[1][i-2:i]))

                f.close()

                try:

                    self.pystlink.cmd(['flash','verify','0x1FFF7800',"serial.bin"])

                    self.pystlink.cmd(['flash','verify','0x1FFF7A0C',"lock.bin"])


                except:
                    pass
                    self.exception("Couldn't Flash the serial Number")

            else:
                #do something if no
                pass      


        self.pystlink.cmd(['flash','erase'])
            
        result = self.pystlink.cmd(['flash','verify','0x08000000',self.fileName])

        self.pystlink.cmd(['reset'])
              
        if(result):

            self.outputText.append("\n\nFirmware Updated Succefully !")   
        	
    def openFileNameDialog(self):
   
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        if self.fileName:
            print(self.fileName)
            filetmpName = self.fileName.split("/")
            self.binaryFileText.setText(filetmpName[len(filetmpName)-1])

    def debug(self, msg, level=3):
        pass
        #print("DEBUG: ",msg)
        #self.outputText.append(msg)   

    def info(self, msg, level=1):
        pass
        #print("INFO: ",msg)
        self.outputText.append(msg)   
    
    def msg(self, msg, level=1):
        pass
        #print("MESSAGE: ",msg)

    def serial(self, msg, level=1):
        pass
        self.serialText.setText("0x"+msg)
        self.myserialtext = self.serialText.toPlainText()
        if(self.myserialtext == "0xffffffff"):
            self.allowSerilaFlash = True
        #print("MESSAGE: ",msg)

    def message(self, msg, level=0):
        pass
        #print("MESSAGE: ",msg)

    def error(self, msg, level=0):
        pass
        #print("ERROR: ",msg)

    def warning(self, msg, level=0):
        pass
        #print("WARNING: ",msg)

    def exception(self, msg, level=0):
        pass
        #print("EXCEPTION: ",msg)
        self.msg.setInformativeText(msg)
        self.msg.exec_() 

    def verbose(self, msg, level=2):
        pass
        #print("VERBOSE: ",msg)
        self.outputText.append(msg)   

    def bargraph_start(self, msg, value_min=0, value_max=100, level=1):
        pass
        #print("BARSTART: ",msg)
        self.progressBar.setMaximum(value_max)
        self.progressBar.setMinimum(value_min)
        self.programLabel.setText(msg)

    def bargraph_update(self, value=0, percent=None):
        pass
        #print("BARUPDATE: ",value)
        self.progressBar.setValue(value)

    def bargraph_done(self):
        pass
        self.programLabel.setText("DONE !")
        
        
if __name__ == '__main__':    
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


